//INES PEREZ ALONSO
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

#include <string.h>
using namespace std;

class CMundo_s  
{
public:
	void Init();
	CMundo_s();
	virtual ~CMundo_s();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
	void CerrarProcesos();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int fd;
//	DatosMemCompartida *memComp;
//	DatosMemCompartida memoria;

	/*int fd_sc;
	char* fifo_sc="./tmp/fifo_servidor_cliente";

	int fd_cs;
      char* fifo_cs="./tmp/fifo_cliente_servidor";*/
	pthread_attr_t attr;
	pthread_t thid1;	

	pid_t pid;

	Socket conexion;
	Socket comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
