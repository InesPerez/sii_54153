#define DATA "./tmp/datMemComp"
#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>

int main(){

	DatosMemCompartida *dat;
	void *d;
	struct stat st;
	int fd;

	fd=open(DATA, O_RDWR);
	if (fd==-1)
		perror("Error al abrir el fichero en bot.cpp. open datMemComp");
	
	if (fstat(fd,&st)==-1)
		perror("Error al obtener el tamaño del fichero en bot.cpp. fstat.");
	
	d=mmap(NULL,st.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, fd,0);
	if(d==(void*)-1)
		perror("Error mmap bot");

	close(fd);
	dat=(DatosMemCompartida *)d;

	while(!dat->endgame){
		
		if (dat->esfera.centro.y>(dat->raqueta1.y1+dat->raqueta1.y2)/2)
			dat->accion=1;
		else if (dat->esfera.centro.y<(dat->raqueta1.y1+dat->raqueta1.y2)/2)
			dat->accion=-1;
		else
			dat->accion=0;
		usleep(25000);	
	}
	return 0;	
}
