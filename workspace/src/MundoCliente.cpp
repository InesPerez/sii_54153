#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <iostream>

#define FIFO "./tmp/fifo"
#define DATA "./tmp/datMemComp"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo_c::CMundo_c()
{
	Init();
}

CMundo_c::~CMundo_c()
{
//close(fd);
//unlink(FIFO);

memComp->endgame=true;
munmap(memComp, sizeof(memoria));

/*close(fd_sc);
unlink(fifo_sc);

close(fd_cs);
unlink(fifo_cs);*/
}

void CMundo_c::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_c::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	
	if(puntos1==3){	
		sprintf(cad, "Ha ganado el Jugador1");
		print(cad,300,300,1,1,1);
	}
	else if(puntos2==3){
		sprintf(cad, "Ha ganado el Jugador2");
		print(cad,300,300,1,1,1);
	}
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);

	for(int i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_c::OnTimer(int value)
{	
	if (puntos1!=3 && puntos2!=3){
		jugador1.Mueve(0.025f);
		jugador2.Mueve(0.025f);
		esfera.Mueve(0.025f);
	}

	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
//		string t="Jugador 2 marca 1 punto, lleva un total de  puntos.\n";	
//		int n=t.length()+puntos2/10+1;
//		char texto[n];
//		sprintf(texto,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos2);	
 //       	write(fd,texto,sizeof(texto));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

//		string t="Jugador 1 marca 1 punto, lleva un total de  puntos.\n";
//                int n=t.length()+puntos1/10+1;
//                char texto[n];
		
//		sprintf(texto,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos1);
 //               write(fd,texto, sizeof(texto));
	}
	memComp->esfera=esfera;
	memComp->raqueta1=jugador1;

	switch(memComp->accion){
		case 1: OnKeyboardDown('w',0,0);break;
		case -1: OnKeyboardDown('s',0,0);break;
		}
	
	char cad[200];
//	read(fd_sc,cad,sizeof(cad));
	if(comunicacion.Receive(cad,sizeof(cad))==-1){
		memComp->endgame=true;
		munmap(memComp, sizeof(memoria));
		exit(0);
	}
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);


}

void CMundo_c::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(cad,"s");break;
	case 'w':sprintf(cad,"w");break;
	
	case 'l':sprintf(cad,"l");break;
	case 'o':sprintf(cad,"o");break;
	}
	
//	write(fd_cs,cad,sizeof(cad));
	comunicacion.Send(cad,sizeof(cad));
}

void CMundo_c::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//comunicación logger
//	fd=creat(FIFO,O_WRONLY);
//	if(fd==-1) perror(FIFO);

	//bot
	int fd_bot;
	void * dat;
	struct stat st;

	fd_bot=open(DATA,O_RDWR|O_CREAT|O_TRUNC,0666);
	if (fd_bot==-1)
		perror("open datMemComp en Mundo.cpp");
 
	write(fd_bot,&memoria, sizeof(memoria));
	if(fstat(fd_bot,&st)==-1)
		perror("Error al obtener el tamaño del fichero en Mundo.cpp");

	dat=mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_bot,0);
	close(fd_bot);

	if (dat==(void *)-1)
		perror("Error al proyectar en memoria: mmap Mundo");
	else{
		memComp=(DatosMemCompartida *)dat;
		memComp->endgame=false;
	}

	//comunicación servidor-cliente
	
/*	if(mkfifo(fifo_sc, 0666)!=0) perror(fifo_sc);

	fd_sc=open(fifo_sc,O_RDONLY);
	if(fd_sc==-1) perror(fifo_sc);

	//comunicación cliente-servidor
	
	if(mkfifo(fifo_cs, 0666)!=0) perror(fifo_cs);

        fd_cs=open(fifo_cs,O_WRONLY);
        if(fd_cs==-1) perror(fifo_cs);*/
        
        //Sockets
        
        char ip[]="127.0.0.1";
        char name[50];
        printf("Introduzca el nombre: ");
        scanf("%s",name);
        
        comunicacion.Connect(ip,8000);
        comunicacion.Send(name,sizeof(name));
        
}
