//INÉS PEREZ ALONSO

// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera():tiempo(0)
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	
}

Esfera::~Esfera()
{
}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x=centro.x+velocidad.x*t;
	centro.y=centro.y+velocidad.y*t;
	
	tiempo++;
	if(tiempo==400 && radio>0.05f){//si han pasado 10 segundos
		radio=radio*0.9;
		tiempo=0;
	}
}
