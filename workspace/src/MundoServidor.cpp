#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <pthread.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <iostream>
#include <signal.h>

#define FIFO "./tmp/fifo"
#define DATA "./tmp/datMemComp"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo_s::CMundo_s()
{
	Init();
}

CMundo_s::~CMundo_s()
{
close(fd);
unlink(FIFO);
//memComp->endgame=true;
//munmap(memComp, sizeof(memoria));
//close(fd_sc);
}

void CMundo_s::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_s::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	
	if(puntos1==3){	
		sprintf(cad, "Ha ganado el Jugador1");
		print(cad,300,300,1,1,1);

		kill(pid,SIGUSR2);
	}
	else if(puntos2==3){
		sprintf(cad, "Ha ganado el Jugador2");
		print(cad,300,300,1,1,1);
		
		kill(pid,SIGUSR2);
	}
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);

	for(int i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_s::OnTimer(int value)
{	
	if (puntos1!=3 && puntos2!=3){
		jugador1.Mueve(0.025f);
		jugador2.Mueve(0.025f);
		esfera.Mueve(0.025f);
	}

	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		string t="Jugador 2 marca 1 punto, lleva un total de  puntos.\n";	
		int n=t.length()+puntos2/10+1;
		char texto[n];
		sprintf(texto,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos2);	
        	write(fd,texto,sizeof(texto));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		string t="Jugador 1 marca 1 punto, lleva un total de  puntos.\n";
                int n=t.length()+puntos1/10+1;
                char texto[n];
		
		sprintf(texto,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos1);
                write(fd,texto, sizeof(texto));
	}
//	memComp->esfera=esfera;
//	memComp->raqueta1=jugador1;

//	switch(memComp->accion){
//		case 1: OnKeyboardDown('w',0,0);break;
//		case -1: OnKeyboardDown('s',0,0);break;
//		}
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
//	write(fd_sc,cad,sizeof(cad));	
	comunicacion.Send(cad,sizeof(cad));
}

void CMundo_s::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
*/
}

void* hilo_comandos(void* d){
	CMundo_s* p=(CMundo_s*) d;
	p->RecibeComandosJugador();
}

void CMundo_s::RecibeComandosJugador(){
	while(1){
		usleep(10);
		char cad[100];
//		read(fd_cs,cad,sizeof(cad));
		comunicacion.Receive(cad,sizeof(cad));
		unsigned char key;
		sscanf(cad,"%c",&key);
		switch(key){
       			case 's': jugador1.velocidad.y=-4;break;
		        case 'w':jugador1.velocidad.y=4;break;
        		case 'o':jugador2.velocidad.y=4;break;
        		case 'l':jugador2.velocidad.y=-4;break;
        	}
	}
}


static void TerminarProceso(int s){
        if (s==SIGUSR2){
                sleep(5);
                unlink(FIFO);
                exit(0);
        }
        else{
                unlink(FIFO);
                cout<<"\nEl proceso ha finalizado debido a la señal: "<<strsignal(s)<<endl;
                exit(s);
        }

}


void CMundo_s::Init()
{	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//comunicación logger
	fd=creat(FIFO,O_WRONLY);
	if(fd==-1) perror(FIFO);

	//bot
//	int fd_bot;
//	void * dat;
//	struct stat st;

//	fd_bot=open(DATA,O_RDWR|O_CREAT|O_TRUNC,0666);
//	if (fd_bot==-1)
//		perror("open datMemComp en Mundo.cpp");
 
//	write(fd_bot,&memoria, sizeof(memoria));
//	if(fstat(fd_bot,&st)==-1)
//		perror("Error al obtener el tamaño del fichero en Mundo.cpp");

//	dat=mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_bot,0);
//	close(fd_bot);

//	if (dat==(void *)-1)
//		perror("Error al proyectar en memoria: mmap Mundo");
//	else{
//		memComp=(DatosMemCompartida *)dat;
//		memComp->endgame=false;
//	}

	//comunicación servidor-cliente
/*	fd_sc=open(fifo_sc,O_WRONLY);
        if(fd_sc==-1) perror(fifo_sc);	

	//comunicación cliente-servidor

	fd_cs=open(fifo_cs,O_RDONLY);
        if(fd_cs==-1) perror(fifo_cs);*/

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
	pthread_create(&thid1,&attr,&hilo_comandos,this);

	//captura de señales
	pid=getpid();
	struct sigaction act;
	act.sa_handler=&TerminarProceso;
	act.sa_flags=0;

	sigaction(SIGINT,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGUSR2,&act,NULL);

	//Sockets
	char ip[]="127.0.0.1";
	if(conexion.InitServer(ip,8000)==-1) perror("Se ha producido un error abriendo el servidor");
	comunicacion=conexion.Accept();
	char cad[100];	
	comunicacion.Receive(cad,sizeof(cad));
	printf("Comunicación con %s.\n",cad);
}


