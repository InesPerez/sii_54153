# Changelog

## [v1.1] 15/10/20
### Changed
-Nombre del autor añadido en las cabeceras.

## [v1.1] 22/10/20
### Added
-Movimiento de Raqueta y Esferas añadido.
-La pelota disminuye su tamaño cada 10s (400 ciclos*0.025), hasta que llega a su tamaño mínimo. Estipulado en radio=0.05f.

## [v1.2] 12/11/2020
### Added
-logger añadido. Ahora nos muestra por la terminal el resultado de la partida cuando hay un cambio.

## [v1.2] 23/11/2020
### Modified
-mundo modificado. Se ha adaptado el tamaño del vector char que envía los datos a logger para que varíe según las decenas de puntos.

## [v1.2] 24/11/2020
### Added
-bot añadido. Controla la raqueta del jugador1.

### Modified
-Funcionalidad añadida. Cuando algún jugador alcanza 3 puntos termina el juego.

## [v1.3] 25/11/2020
### Modified
-Bit de fin de juego se ha añadido a DatosMemCompartida para poder establecer una comunicación sencilla con el bot y cerrar el proceso.

## [v1.3] 25/11/2020
### Added
-Se han añadido el cliente y el servidor. Falla el bot (funciona una vez de cierra el servidor).

## [v1.3] 05/12/2020
### Modified
-Configuración cliente-servidor y servidor-cliente completada.

### Added
-Captura de señal SIGUSR2 al terminar el juego.

## [v1.4] 10/12/2020
### Added
-Captura de señales completa.

## [v1.5] 12/12/2020
### Added
-Clase Socket añadida.
-Implementación del juego mediante servidores.
