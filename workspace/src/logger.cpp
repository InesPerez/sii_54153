//INES PÉREZ ALONSO

#define FIFO "./tmp/fifo"
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

int main(){
char texto;
int fd;

if(mkfifo(FIFO, 0666)!=0){
	perror(FIFO);
	return 1;
}

fd=open(FIFO,O_RDONLY);
if(fd==-1){
	perror(FIFO);
	return 1;
}
	
while(read(fd,&texto,1)==1){
	write(1,&texto, 1);
}
close(fd);
unlink(FIFO);
return 0;
}




